//toolbar工具条
import QtQuick 2.1
Item {
    id: toolbar
    //是否带返回按钮
    property bool backButtonShow:true
    //标题
    property string title: ""
    //标题文字颜色
    property alias fontColor: titleArea.color
    //返回按钮的事件
    signal back();
    ColorArea{
           anchors.fill:parent
           Text{
                id:titleArea
                text:title
                visible: title != ""
                font.bold: true
                font.pointSize: 20
                anchors.centerIn: parent
           }
           Item{
               height:parent.height - 20
               width: parent.height - 20
               visible: backButtonShow
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: parent.left
               anchors.leftMargin: 10
               Image {
                   id: back
                   width: parent.width - 10
                   height: parent.width - 10
                   anchors.centerIn: parent
                   source: "qrc:/tool/resource/images/back.png"
               }
               Rectangle{
                    anchors.centerIn:  back
                    height: back.height-20
                    width: back.height-20
                    color:"#cccccc"
                    opacity: .2
                    radius: 10
                    visible: imgMouse.pressed
               }
               MouseArea{
                   id:imgMouse
                   anchors.fill: parent
                   onClicked: {
                        toolbar.back();
                   }
               }
           }
    }
}
