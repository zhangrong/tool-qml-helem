//按钮
import QtQuick 2.1
Item {
    id: colorAreaContainer
    //主色调
    property string mainColor:""// colorArea.color
    //上层颜色
    property string topColor: ""//top.color
    //下层颜色
    property string bottomColor: ""//bottom.color
    //边框
    property string border: ""//colorArea.border
    anchors.fill: parent
    Rectangle {
        id: colorArea
        color:mainColor == "" ? "#cccccc" : mainColor
        border.color: "#ccc"
        border.width: 1
        anchors.fill:parent
        Rectangle {
            id:top
            height: 1
            color:topColor == "" ? "white" : topColor
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
        }
        Rectangle {
            id:bottom
            height: 1
            color:bottomColor == "" ? "#cccccc":bottomColor
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.left: parent.left
            anchors.right: parent.right
        }
        gradient: Gradient {
            GradientStop { position: 0 ; color: topColor == ""  ? "white" : topColor}
            GradientStop { position: 1 ; color: bottomColor == "" ?"#cccccc": bottomColor}
        }
    }
}
