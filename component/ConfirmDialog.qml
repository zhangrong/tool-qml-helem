import QtQuick 2.1
import QtQuick.Controls 1.1
Item{
    id:diolag
    anchors.fill: parent
    //取消按钮文字
    property alias leftButtonText: cancel.text
    //确认按钮文字
    property alias rightButtonText: confirm.text
    //文字信息
    property alias text:msg.text
    //提交方法
    signal submit()
    Rectangle {
        id: overlay
        width:parent.width
        height:parent.height
        color:"#332f2f"
        opacity: 0.4
        z: content.z - 1
        MouseArea{
            anchors.fill: parent
        }
    }
    Rectangle {
        id: content
        width: parent.width * 0.6
        height:msgArea.height + 60
        anchors.centerIn: parent
        color: "white"
        z: 20
        border.width: 5
        border.color: "#847a7a"
        radius: 10
        MouseArea{
            anchors.fill: parent
        }
        Item{
            id:msgArea
            width: parent.width - 10
            x:5
            y:5
            height:msg.height+10
            anchors.horizontalCenter: parent.horizontalCenter
            Text{
                id:msg
                anchors.centerIn:parent
                width: parent.width
                horizontalAlignment:Text.AlignHCenter//文字居中
                wrapMode: Text.WordWrap;
                font.pointSize: 16
                elide: Text.ElideRight
            }
        }
        Row{
            anchors.top:msgArea.bottom
            width:msgArea.width
            height:50
            x:10
            spacing: 10
            Rectangle{
                width: parent.width/2- 10
                anchors.verticalCenter: parent.verticalCenter
                x:5
                y:5
                radius: 5
                height:40
                border.color: "#a29c9c"
                gradient: Gradient {
                     GradientStop {
                         position: 0.00;
                         color: cancelMouse.pressed ? "#dddddd":"#cccccc";
                     }
                     GradientStop {
                         position: 1.00;
                         color:"#ffffff";
                     }
                 }
                Text{
                    id:cancel
                    //text:"取消"
                    anchors.centerIn: parent
                    font.bold: true
                    font.pixelSize: 16
                }
                MouseArea{
                    id:cancelMouse
                    anchors.fill: parent
                    onClicked: {
                        diolag.destroy();
                    }
                }
            }
            Rectangle{
                width: parent.width /2- 10
                x:5
                anchors.verticalCenter: parent.verticalCenter
                y:5
                 radius: 5
                height:40
                gradient: Gradient {
                     GradientStop {
                         position: 0.00;
                         color: subMouse.pressed ? "#dddddd":"#cccccc";
                     }
                     GradientStop {
                         position: 1.00;
                         color:"#ffffff";
                     }
                 }
                 border.color: "#a29c9c"
                 MouseArea{
                     id:subMouse
                     anchors.fill: parent
                     onClicked: {
                        diolag.submit();
                        diolag.destroy();
                     }
                 }
                 Text{
                     id:confirm
                     //text:"确定"
                     anchors.centerIn: parent
                     font.bold: true
                     font.pixelSize: 16
                 }
             }
        }
    }
}
