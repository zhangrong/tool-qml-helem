﻿//计时器
import QtQuick 2.1
import QtQuick.Window 2.1
Item {
    id: timer
    //间隔时间
    property alias interval: time.interval
    //运行状态
    property alias running: time.running
    //是否重复
    property alias repeat: time.repeat
    //出发事件
    signal triggered
    Timer {
        id:time
        onTriggered: {
           timer.triggered()
        }
     }
}

