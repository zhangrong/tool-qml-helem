import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Window 2.1
import "qrc:/library/component" as Components
import custom 1.0

Item {
    id: dialog
    z: parent.z + 3
    anchors.fill: parent
    //内容区域的宽度
    property alias contentWidth: content.width
    //内容区域的高度
    property alias contentHeight: content.height
    //标题
    property alias title: titleArea.text
    //上边距
    property real topMargin: 0
    //内容区居中
    property bool centered: false
    //下边距
    property real bottomMargin: 0
    //左边距
    property real leftMargin: 0
    //右边距
    property real rightMargin: 0
    //子组件
    property alias items: itemsArea.children
    //触摸隐藏mask
    property bool hideOnMaskTap : false
    //笼罩
    property bool modal: true
    function close() {
        dialog.closed();
        dialog.destroy();
    }
   //关闭事件
    signal closed();
    Component.onCompleted: {
        if(centered){
            content.anchors.centerIn = dialog
        }else{
            if(topMargin>0){
                    content.anchors.top = dialog.top;
                    content.anchors.topMargin = topMargin
            }
            if(bottomMargin>0){
                    content.anchors.bottom = dialog.bottom;
                    content.anchors.bottomMargin = bottomMargin
            }
            if(leftMargin>0){
                    content.anchors.left = dialog.left;
                    content.anchors.leftMargin = leftMargin
            }
            if(rightMargin>0){
                    content.anchors.right = dialog.right;
                    content.anchors.rightMargin = rightMargin
            }
        }
    }
    Rectangle {
        id: overlay
        width:parent.width
        height:parent.height
        color: 'black'
        opacity: modal?0.5:0
        z: parent.z-1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(hideOnMaskTap){
                    dialog.close()
                }
            }
        }
    }
    Rectangle {
        id: content
        z: parent.z + 1
        MouseArea{
            anchors.fill: parent
        }
        Components.SToolBar{
            id:titleArea
            anchors.top: parent.top
            width: parent.width
            visible: title == ""?false:true
            z:parent.z +1
            text:title
            Components.SImageButton{
                url:"qrc:/library/resource/images/close.png"
                anchors.right: parent.right
                anchors.rightMargin: 20
                anchors.verticalCenter: parent.verticalCenter
                height: parent.height*.9
                width: parent.height*.9
                onClicked: dialog.close();
            }
        }
        Item {
            id: items
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: title == ""?parent.top:titleArea.bottom
            anchors.bottom: parent.bottom
            children: Rectangle {
                id: itemsArea
                anchors.fill: parent
                visible: itemsArea.children.length == 0 ? false : true
            }
        }
    }
}
