#ifndef LANGUAGE_H
#define LANGUAGE_H
#include <QObject>
class Language : public  QObject
{
    Q_OBJECT
    Q_PROPERTY(QString language READ  getLanguage WRITE setLanguage NOTIFY languageChange)
public:
    explicit Language(QObject *parent = 0);
    QString getLanguage(void) const;
    void setLanguage(const QString &language);
signals:
    void languageChange(void);
private:
    QString  new_Language;
};
#endif // LANGUAGE_H
