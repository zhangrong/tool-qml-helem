#include "language.h"
#include "qtquick2applicationviewer.h"
#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include <QtCore>
Language::Language(QObject *parent) :
    QObject(parent),
    new_Language("zh_CN")
{
}


QString Language::getLanguage(void) const
{
    return new_Language;
}
void Language::setLanguage(const QString & language)
{
    new_Language = language;
    emit languageChange();
    QDir dir(":/translations");
    QStringList fileNames = dir.entryList(QStringList("*.qm"), QDir::Files, QDir::Name);
    QMutableStringListIterator listIter(fileNames);
    while (listIter.hasNext()) {
        listIter.next();
        listIter.setValue(dir.filePath(listIter.value()));
    }
    for (int i = 0; i < fileNames.size(); ++i) {
       if(fileNames[i].indexOf(language)>0){
           QTranslator *translator = new QTranslator(qApp);
           translator->load(fileNames[i]);
           qApp->installTranslator(translator);
           QSettings *settings = new QSettings("language.ini", QSettings::IniFormat);
           settings->setIniCodec(QTextCodec::codecForName("UTF-8"));
           settings->setValue("language",language);
       }
    }
}
