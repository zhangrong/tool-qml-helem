#include "gziputil.h"

/**
 * @brief GzipUtil::GzipUtil
 * @param parent
 */
GzipUtil::GzipUtil(QObject *parent) :QObject(parent)
{
}

/**
 * @brief GzipUtil::encode
 * @param source
 * @return
 */
QString GzipUtil::encode(QString source)
{
    QByteArray result(source.toUtf8());
    QString value = gzipCompress(result).toBase64().data();
    return value.replace("/","!");
}

/**
 * @brief GzipUtil::decode
 * @param source
 * @return
 */
QString GzipUtil::decode(QString source)
{
    //服务端无法正常解析“/”,所以用“！”代替，正常解析前要做个转换
    QByteArray in = source.replace('!','/').toUtf8();
    //先解密base64
    QByteArray ins = QByteArray::fromBase64(in, QByteArray::Base64Encoding);
    return degzip(ins).data();
}

/**
 * @brief httpgzdecompress
 * @param zdata
 * @param nzdata
 * @param data
 * @param ndata
 * @return
 */
int httpgzdecompress(Byte *zdata, uLong nzdata, Byte *data, uLong *ndata)
{
    int err = 0;
    z_stream d_stream = {0}; /* decompression stream */
    static char dummy_head[2] = {
        0x8 + 0x7 * 0x10,
        (((0x8 + 0x7 * 0x10) * 0x100 + 30) / 31 * 31) & 0xFF,
    };
    d_stream.zalloc = (alloc_func)0;
    d_stream.zfree = (free_func)0;
    d_stream.opaque = (voidpf)0;
    d_stream.next_in  = zdata;
    d_stream.avail_in = 0;
    d_stream.next_out = data;
    if(inflateInit2(&d_stream, 47) != Z_OK) { return -1; }
    while (d_stream.total_out < *ndata && d_stream.total_in < nzdata) {
        d_stream.avail_in = d_stream.avail_out = 1; /* force small buffers */
        if((err = inflate(&d_stream, Z_NO_FLUSH)) == Z_STREAM_END) break;
        if(err != Z_OK ) {
            if(err == Z_DATA_ERROR) {
                d_stream.next_in = (Bytef*) dummy_head;
                d_stream.avail_in = sizeof(dummy_head);
                if((err = inflate(&d_stream, Z_NO_FLUSH)) != Z_OK) {
                    return -1;
                }
            }
            else {
                return -1;
            }
        }
    }
    if(inflateEnd(&d_stream) != Z_OK) return -1;
    *ndata = d_stream.total_out;
    return 0;
}

/**
 * @brief httpgzcompress
 * @param data
 * @param ndata
 * @param zdata
 * @param nzdata
 * @return
 */
int httpgzcompress(Bytef *data, uLong ndata, Bytef *zdata, uLong *nzdata)
{
    z_stream c_stream;
    int err = 0;
    if(data && ndata > 0)
    {
        c_stream.zalloc = (alloc_func)0;
        c_stream.zfree = (free_func)0;
        c_stream.opaque = (voidpf)0;
        if(deflateInit2(&c_stream, Z_DEFAULT_COMPRESSION, Z_DEFLATED,
                        MAX_WBITS + 16, 8, Z_DEFAULT_STRATEGY) != Z_OK) return -1;
        c_stream.next_in  = data;
        c_stream.avail_in  = ndata;
        c_stream.next_out = zdata;
        c_stream.avail_out  = *nzdata;
        while (c_stream.avail_in != 0 && c_stream.total_out < *nzdata) {
            if(deflate(&c_stream, Z_NO_FLUSH) != Z_OK) return -1;
        }
        if(c_stream.avail_in != 0) return c_stream.avail_in;
        for (;;) {
            if((err = deflate(&c_stream, Z_FINISH)) == Z_STREAM_END) break;
            if(err != Z_OK) return -1;
        }
        if(deflateEnd(&c_stream) != Z_OK) return -1;
        *nzdata = c_stream.total_out;
        return 0;
    }
    return -1;
}

/**
 * @brief GzipUtil::gzipCompress
 * @param in
 * @return
 */
QByteArray GzipUtil::gzipCompress(const QByteArray &in)
{
    QByteArray dest(compressBound(in.size()) + 7, 0);
    uLongf destLen = dest.size();
    int r1 = httpgzcompress((Bytef*)in.data(), in.size(), (Bytef*)dest.data(),&destLen);
    dest.resize(destLen);
    return dest;
}

/**
 * @brief GzipUtil::gzipUncompress
 * @param data
 * @param out
 * @return
 */
bool GzipUtil::gzipUncompress(const QByteArray &data, QByteArray &out)
{
    if (data.size()<=8) {
        return false;
    }
    ulong orign_crc = *((ulong*)(data.data() + data.length() - 8));
    ulong len =*((ulong*)(data.data() + data.length() - 4));
    out.resize(len);
    int ret = httpgzdecompress((Bytef *)data.data(), data.length(),(Bytef *)out.data(), &len); //(Bytef *)(data.data()+10), data.length()-10-8);
    if (ret!=Z_OK)  {
        qWarning("gzipUncompress error: %d", ret);
        return false;
    }
    uLong crc = crc32(0L, Z_NULL, 0);
    crc = crc32(crc, (Bytef *)(out.data()), out.length());
    if (crc==orign_crc) {
        return true;
    } else {
        qWarning("gzipUncompress crc check sum fail");
        out.clear();
        return false;
    }
}

/**
 * @brief GzipUtil::gzipUncompress
 * @param data
 * @return
 */
QByteArray GzipUtil::gzipUncompress(const QByteArray &data)
{
    QByteArray out;
    gzipUncompress(out);
    return out;

}

/**
 * 解密gzip
 * @brief GzipUtil::degzip
 * @param srcData
 * @return
 */
QByteArray GzipUtil::degzip(QByteArray srcData)
{
    z_stream ungzipstream;
    unsigned char buffer[4096];
    int ret = 0;
    QByteArray unComprData;

    ungzipstream.zalloc = Z_NULL;
    ungzipstream.zfree = Z_NULL;
    ungzipstream.opaque = Z_NULL;
    ungzipstream.avail_in = srcData.size();
    ungzipstream.next_in = (Bytef*)srcData.data();
    ret = inflateInit2(&ungzipstream,47);
    if(ret != Z_OK) {
        qDebug() <<"!Z_OK";
        return QByteArray();
    }

    do {
        memset(buffer,0,4096);
        ungzipstream.avail_out = 4096;
        ungzipstream.next_out = buffer;
        ret = inflate(&ungzipstream,Z_NO_FLUSH);
        switch(ret) {
        case Z_NEED_DICT:
            ret = Z_DATA_ERROR;
        case Z_DATA_ERROR:
        case Z_MEM_ERROR:
            return QByteArray();
        }

        if(ret != Z_FINISH) {
            unComprData.append((char*)buffer);
        }

    } while(ungzipstream.avail_out == 0);
    return unComprData;
}

