#ifndef GZIPUTIL_H
#define GZIPUTIL_H
#define GZIP_BUF_SIZE 65535
#define GZIP_BUF_SIZE_MAX 163240

#include <QObject>
#include <QDebug>
#include <zlib.h>
#include <QDebug>

class  GzipUtil : public QObject
{
    Q_OBJECT

public:
    explicit GzipUtil (QObject* parent = 0);
    //加密
    Q_INVOKABLE QString encode(QString source);
    //解密
    Q_INVOKABLE QString decode(QString source);

    //单纯数据解密为gzip
    Q_INVOKABLE QByteArray degzip(QByteArray str);

    Q_INVOKABLE QByteArray gzipCompress(const QByteArray &in);

    Q_INVOKABLE bool gzipUncompress(const QByteArray &data, QByteArray &out);

    Q_INVOKABLE QByteArray gzipUncompress(const QByteArray &data);
};

#endif // GZIPUTIL_H
