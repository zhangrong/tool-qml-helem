#include "imagehandler.h"
#include <QGraphicsObject>
#include <QImage>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QDebug>
#include <QZXing.h>
ImageHandler::ImageHandler(QObject *parent) :
    QObject(parent)
{
}

QImage ImageHandler::extractQImage(QObject *imageObj,
                                   const double offsetX, const double offsetY,
                                   const double width, const double height)
{


    QImage *image= new QImage("qrc:/library/resource/images/qrcode.png");
    QPixmap pixmap = QPixmap::fromImage(*image);

       if(pixmap.isNull()){
            qDebug() << "还未截图";
       }else{
           QZXing decoder;
           QString qrmsg = decoder.decodeImage(pixmap.toImage());

           if(qrmsg.isEmpty()){

                qDebug() << "请截取完整的二维码";

           }else{

                qDebug() << "识别成功-已经复制到剪切板,内容为："+qrmsg;

           }

       }


    QImage img;
 //   QImage img(item->boundingRect().size().toSize(), QImage::Format_RGB32);

    img.fill(QColor(255, 255, 255).rgb());

   // QPainter painter(&img);
    //QStyleOptionGraphicsItem styleOption;
    //img->paint(&painter, &styleOption);


    if(offsetX == 0 && offsetY == 0 && width == 0 && height == 0)
        return img;
    else
    {
        return img.copy(offsetX, offsetY, width, height);
    }
}

void ImageHandler::save(QObject *imageObj, const QString &path,
                        const double offsetX, const double offsetY,
                        const double width, const double height)
{
    QImage img = extractQImage(imageObj, offsetX, offsetY, width, height);
    img.save(path);
}
