﻿#ifndef DOWNFILE_H
#define DOWNFILE_H

#include <QObject>
#include <QtCore/QUrl>
#include <QFile>
#include <QTextStream>
#include <QtCore/QFile>
#include <QtCore/QObject>
#include <QtCore/QQueue>
#include <QtCore/QStringList>
#include <QtCore/QTime>
#include <QtNetwork/QNetworkAccessManager>
#include <QDebug>


class DownFile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path WRITE setPath USER true)

    //以下为下载图片需要的类
    // Makes error messages available to the UI
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY errorMessageChanged)

    // Makes status messages available to the UI
    Q_PROPERTY(QString statusMessage READ statusMessage NOTIFY statusMessageChanged)

    // Makes the number of currently running downloads available to the UI
    Q_PROPERTY(int activeDownloads READ activeDownloads NOTIFY activeDownloadsChanged)

    // Makes the total number of bytes of the current download available to the UI
    Q_PROPERTY(int progressTotal READ progressTotal NOTIFY progressTotalChanged)

    // Makes the already downloaded number of bytes of the current download available to the UI
    Q_PROPERTY(int progressValue READ progressValue NOTIFY progressValueChanged)

    // Makes the progress message available to the UI
    Q_PROPERTY(QString progressMessage READ progressMessage NOTIFY progressMessageChanged)

public:
    explicit DownFile(QObject *parent = 0);
    //初始化文件路径
    QString path() const;

    void setPath(const QString &filename);
//以下为下载图片需要的类
    QString errorMessage() const;
    QString statusMessage() const;
    int activeDownloads() const;
    int progressTotal() const;
    int progressValue() const;
    QString progressMessage() const;

public Q_SLOTS:
      //下载图片路径
      void downImageByUrl(const QString &imageName);

      //通过路径写入文件
      void writeFileByUrl(const QString &filename,const QString &data);
      //通过路径读出文件
      QString readFileByUrl(const QString &filename);

      //以下为下载图片需要的类
      // This method starts the next download from the internal job queue
      void startNextDownload();

      // This method is called whenever the progress of the current download job has changed
      void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);

      // This method is called whenever a download job has finished
      void downloadFinished();

      // This method is called whenever the current download job received new data
      void downloadReadyRead();

Q_SIGNALS:
      //以下为下载图片需要的类
    // The change notification signals of the properties
    void errorMessageChanged();
    void statusMessageChanged();
    void imageurlMessageChanged();
    void activeDownloadsChanged();
    void progressTotalChanged();
    void progressValueChanged();
    void progressMessageChanged();
private:

    QString m_path;

    //以下为下载图片需要的类
    // Enqueues a new download to the internal job queue
    void append(const QUrl &url);

    // This method determines a file name that can be used to save the given URL
    QString saveFileName(const QUrl &url);

    // A helper method to collect error messages
    void addErrorMessage(const QString &message);

    // A helper method to collect status messages
    void addStatusMessage(const QString &message);

    // The network access manager that does all the network communication
    QNetworkAccessManager m_manager;

    // The internal job queue
    QQueue<QUrl> m_downloadQueue;

    // The currently running download job
    QNetworkReply *m_currentDownload;

    // The file where the downloaded data are saved
    QFile m_output;

    // The time when the download started (used to calculate download speed)
    QTime m_downloadTime;

    // The number of finished download jobs
    int m_downloadedCount;

    // The total number of download jobs
    int m_totalCount;

    // The total number of bytes to transfer for the current download job
    int m_progressTotal;

    // The number of already transferred bytes for the current download job
    int m_progressValue;

    // A textual representation of the current progress status
    QString m_progressMessage;

    // The list of error messages
    QStringList m_errorMessage;

    // The list of status messages
    QStringList m_statusMessage;
};

#endif // DOWNFILE_H
